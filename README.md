# Basic Calculator App

This basic calculator app can perform the four basic arithmetic operations(addition, subtraction, multiplication and division)
Created using react with typescript and styled-components

# To Run this App

### `cd basic-calculator`

### `npm i`

### `npm start`

# To lint this App

### `npm run lint`

# Author

Abhijeet Pareek

# More Features

Can display the _`whole expression`_ to be calculated instead of just showing a single value along with that we can add a _`backspace`_ button. 

Calculator should also be accessable with a keyboard not only by clicking
