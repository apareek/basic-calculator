import React from 'react';
import styled from 'styled-components';

interface IButtonProps {
  background?: string;
}

const Button = styled.button<IButtonProps>`
  background: ${(props) => (props.background ? props.background : '#f3f1f1')};
  border-radius: 3px;
  border: none;
  color: black;
  width: 50px;
  height: 30px;
  font-size: 16px;
  cursor: pointer;

  :active {
    background-color: #fff;
  }
`;

interface ICalculatorButton {
  value: string;
  type: string;
  name: string;
  click: (type: string, name: string) => void;
  background?: string;
}

const CalculatorButton: React.FC<ICalculatorButton> = ({
  value,
  background,
  click,
  type,
  name,
}) => {
  const handleClick = () => {
    click(type, name);
  };

  return (
    <Button onClick={handleClick} background={background}>
      <div dangerouslySetInnerHTML={{__html: value}} />
    </Button>
  );
};

export default CalculatorButton;
