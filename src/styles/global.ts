import { createGlobalStyle } from 'styled-components';

export const MyGlobalStyle = createGlobalStyle`
  * {
      padding : 0;
      margin: 0;
      outline: 0;
      box-sizing: border-box;
  }

  #root{
      margin: 0 auto;
  }
`;
