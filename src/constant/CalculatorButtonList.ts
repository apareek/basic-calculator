export const CalculatorButtonList = [
  [{value: 'C', name: 'clear', type: 'action'}],
  [
    {
      value: '7',
      name: '7',
      type: 'number',
    },
    {
      value: '8',
      name: '8',
      type: 'number',
    },
    {
      value: '9',
      name: '9',
      type: 'number',
    },
    {
      value: '&divide;',
      name: '/',
      type: 'action',
    },
  ],
  [
    {
      value: '4',
      name: '4',
      type: 'number',
    },
    {
      value: '5',
      name: '5',
      type: 'number',
    },
    {
      value: '6',
      name: '6',
      type: 'number',
    },
    {
      value: '&times;',
      name: '*',
      type: 'action',
    },
  ],
  [
    {
      value: '1',
      name: '1',
      type: 'number',
    },
    {
      value: '2',
      name: '2',
      type: 'number',
    },
    {
      value: '3',
      name: '3',
      type: 'number',
    },
    {
      value: '&ndash;',
      name: '-',
      type: 'action',
    },
  ],
  [
    {
      value: '0',
      name: '0',
      type: 'number',
    },
    {
      value: '.',
      name: '.',
      type: 'number',
    },
    {
      value: '+',
      name: '+',
      type: 'action',
    },
    {
      value: '=',
      background: 'orange',
      name: '=',
      type: 'action',
    },
  ],
];
