import React from 'react';
import styled from 'styled-components';
import Calculator from '../Calculator/Calculator';

const Header = styled.div`
  text-align: center;
  padding: 20px;
  font-size: 24px;
  font-family: 'Times New Roman', Times, serif;
  border-bottom: 2px solid #dcdcdc;
`;

const Home: React.FC = () => {
  return (
    <>
      <Header>Calculator</Header>
      <Calculator />
    </>
  );
};

export default Home;
