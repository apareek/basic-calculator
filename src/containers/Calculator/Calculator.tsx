import React, {useState} from 'react';
import styled from 'styled-components';
import CalculatorButton from '../../components/CalculatorButton';
import {evaluate} from 'mathjs';
import {CalculatorButtonList} from '../../constant/CalculatorButtonList';

const CalculatorBody = styled.div`
  background-color: rgb(0 0 0 / 72%);
  border-radius: 10px;
  box-shadow: 0px 5px 10px #000;
  width: 300px;
  margin: 50px auto;
  position: relative;
  padding: 20px;
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 20px;
`;

const DisplayScreen = styled.div`
  width: 100%;
  padding: 18px;
  border: 1px solid black;
  border-radius: 5px;
  background-color: rgb(255 255 255);
  font-size: 18px;
  white-space: nowrap;
  display: flex;
  justify-content: flex-end;
  overflow-x: auto;
`;

const Calculator: React.FC = () => {
  const [display, setDisplay] = useState<string>('-');
  const [expression, setExpression] = useState<string>('');
  const [result, setResult] = useState<boolean>(false);

  const calculate = () => {
    try {
      return '' + (evaluate(expression) || 0);
    } catch (e) {
      return 'error';
    }
  };

  const handleClick = (type: string, name: string) => {
    if (type === 'number') {
      if (result) {
        setExpression(name);
        setDisplay(name);
        setResult(false);
      } else {
        setExpression((prevValue) => prevValue?.concat(name) || '');
        display === '-'
          ? setDisplay(name)
          : setDisplay((prevValue) => prevValue?.concat(name) || '-');
      }
    } else {
      if (result) {
        setResult(false);
      }

      if (name === '=') {
        const finalResult = calculate();
        setResult(true);
        setExpression(finalResult);
        setDisplay(finalResult);
      } else if (name === 'clear') {
        setExpression('');
        setDisplay('-');
      } else {
        setDisplay('-');
        expression.length > 0 &&
          setExpression((prevValue) => prevValue?.concat(name) || '');
      }
    }
  };

  return (
    <CalculatorBody>
      <DisplayScreen>{display}</DisplayScreen>

      {CalculatorButtonList.map((row, index) => {
        return (
          <Row key={index}>
            {row.map((buttonDetail) => {
              return (
                <CalculatorButton
                  click={handleClick}
                  value={buttonDetail.value}
                  name={buttonDetail.name}
                  type={buttonDetail.type}
                  key={buttonDetail.name}
                  background={buttonDetail?.background || undefined}
                />
              );
            })}
          </Row>
        );
      })}
    </CalculatorBody>
  );
};

export default Calculator;
