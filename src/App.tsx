import React from 'react';
import Home from './containers/Home/Home';
import {MyGlobalStyle} from './styles/global';

const App: React.FC = () => {
  return (
    <>
      <MyGlobalStyle />
      <Home />
    </>
  );
};

export default App;
